Moved to GitHub as most Zed development is done there
https://github.com/WeetHet/typst.zed

# Typst

- Tree Sitter: [tree-sitter-typst](https://github.com/uben0/tree-sitter-typst/)
- Language Server: [tinymist](https://github.com/Myriad-Dreamin/tinymist/)
